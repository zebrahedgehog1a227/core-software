#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>
#include <sys/wait.h>

void operator_pipe(char **parsed, char **parsed_pipe)
{
	int pipefd[2];
	pid_t program1, program2;

	if (pipe(pipefd) < 0 )
		fprintf(stderr, "pipe(); failed\n");

	program1 = fork();

	if (program1 < 0)
		fprintf(stderr, "fork(); failed\n");

	if (program1 == 0) {
		close(pipefd[0]);
		dup2(pipefd[1], STDOUT_FILENO);

		close(pipefd[1]);

		if (execvp(parsed[0], parsed) < 0)
			fprintf(stderr, "command not found\n");
	}

	else {
		program2 = fork();

		if (program2 < 0)
			fprintf(stderr, "fork(); failed\n");

		if (program2 == 0) {
			close(pipefd[1]);
			dup2(pipefd[0], STDIN_FILENO);

			close(pipefd[0]);

			if (execvp(parsed_pipe[0], parsed_pipe) < 0)
				fprintf(stderr, "command not found\n");
		}

		else {
			wait(NULL);
			wait(NULL);
		}
	}
}

int main(int argc, char *argv[])
{
	if (argc < 2) {
		fprintf(stderr, "Usage: program | program\n");
		exit(-1);
	}

	operator_pipe(&argv[1], &argv[3]);
	return 0;
}

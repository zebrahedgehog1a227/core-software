#include <stdio.h>
#include <stdlib.h>

#include <string.h>
#include <unistd.h>

void createsoft(char *origin, char *dest)
{
	if (symlink(origin, dest) == -1)
		fprintf(stderr, "symlink(); failed\n");
}

void createhard(char *origin, char *dest)
{
	if (link(origin, dest) == -1)
		fprintf(stderr, "link(); failed\n");
}

int main(int argc, char *argv[])
{
	if (argc < 3) {
		fprintf(stderr, "Usage: ln [-s] origin dest");
		exit(-1);
	}

	else {
		if (strncmp(argv[1], "-s", 2) == 0)
			createsoft(argv[2], argv[3]);

		else
			createhard(argv[1], argv[2]);
	}

	return 0;
}

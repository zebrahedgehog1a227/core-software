#include <stdio.h>
#include <stdlib.h>

int main(int argc, const char *argv[])
{
	if (argc > 2) {
		fprintf(stderr, "Usage: mv origin dest\n");
		exit(-1);
	}

	else if (rename(argv[1], argv[2]) == -1)
		printf("rename(); failed\n");

	return 0;
}

#include <stdio.h>
#include <stdlib.h>

#include <string.h>
#include <sys/stat.h>

int main(int argc, char *argv[])
{
	short i, fin;
	char *slash;

	struct stat directory = {0};

	if (argc < 2) {
		fprintf(stderr, "Usage: mkdir directories\n");
		exit(-1);
	}

	else {
		for (i = 0; i < argc; i++) {
			slash = argv[i];

			slash += strspn(slash, "/");
			slash += strcspn(slash, "/");

			fin = (*slash == '\0');
			*slash = '\0';

			if (mkdir(argv[i], 0777) == 0) {
				fprintf(stderr, "could not create directory\n");
				exit(-1);
			}

			else {
				if (stat(argv[i], &directory) == -1) {
					fprintf(stderr, "directory already exists\n");
					exit(-1);
				}

				if (!S_ISDIR(directory.st_mode)) {
					fprintf(stderr, "is already a file\n");
					exit(-1);
				}
			}

			if (fin)
				break;

			*slash = '/';
		}
	}

	return 0;
}

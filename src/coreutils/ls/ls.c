#include <stdio.h>
#include <stdlib.h>

#include <string.h>
#include <stdbool.h>

#include <sys/stat.h>
#include <dirent.h>

bool hidden(char *filename)
{
	return (strncmp(filename, ".", strlen(".")) == 0);
}

int main(int argc, char *argv[])
{
	char *dirname = ".";
	bool show_hidden = false;

	if (argc >= 2) {
		dirname = argv[argc - 1];

		if (strcmp(argv[1], "-a") == 0) {
			show_hidden = true;

			if (argc == 2)
				dirname = ".";
		}
	}

	struct stat file_type;
	struct dirent *contents = NULL;

	DIR *directory = opendir(dirname);

	if (directory == NULL) {
		fprintf(stderr, "no such file or directory\n");
		exit(1);
	}

	while ((contents = readdir(directory)) != NULL) {
		if ((hidden(contents->d_name) && !show_hidden))
			continue;

		printf("%s\n", contents->d_name);
	}

	closedir(directory);
	return 0;
}

#include <stdio.h>
#include <stdlib.h>

#include <string.h>
#include <sys/stat.h>

#include <dirent.h>
#include <unistd.h>

short is_directory(char *path)
{
	struct stat path_stat;
	stat(path, &path_stat);

	return S_ISREG(path_stat.st_mode);
}

void copy_file(char *origin, char *dest)
{
	FILE *input = fopen(origin, "r");
	FILE *output = fopen(dest, "w");

	char content[256];

	while (fgets(content, sizeof(content), input) != NULL)
		fputs(content, output);
}

void copy_directory(char origin[], char dest[])
{
	struct stat dir = {0};
	struct dirent *contents = NULL;

	DIR *directory;
	char new_dest[256];

	short i;

	if ((directory = opendir(origin)) == NULL) {
		fprintf(stderr, "couldn't open directory\n");
		exit(-1);
	}

	if (stat(dest, &dir) != -1)
		chdir(dest);

	else {
		if (mkdir(dest, 0777) == -1) {
			fprintf(stderr, "couldn't create directory\n");
			exit(-1);
		}

		else if (chdir(dest) == -1) {
			fprintf(stderr, "couldn't cd into directory\n");
			exit(-1);
		}
	}

	strcpy(new_dest, dest);

	for (i = 1; i < strlen(contents->d_name) -1; i++) {
		while ((contents = readdir(directory)) != NULL) {
			if (is_directory(&contents->d_name[i]) == 0) {
				strcat(new_dest, &contents->d_name[i]);
				copy_directory(&contents->d_name[i], new_dest);
			}

			else
			copy_file(&contents->d_name[i], dest);
		}
	}

	closedir(directory);
}

int main(int argc, char *argv[])
{
	if (argc < 2) {
		fprintf(stderr, "Usage: cp [-r] origin dest\n");
		exit(-1);
	}

	else {
		if (strncmp(argv[1], "-r", 2) == 0)
			copy_directory(argv[2], argv[3]);

		else
			copy_file(argv[1], argv[2]);
	}

	return 0;
}

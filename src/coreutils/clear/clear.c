#include <stdio.h>
#include <stdlib.h>

int main()
{
	if (printf("\033[H\033[J") != 0) {
		fprintf(stderr, "escape sequence \"\033[H\033[J\" not accepted\n");
		exit(-1);
	}

	return 0;
}

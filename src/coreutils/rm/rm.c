#include <stdio.h>
#include <stdlib.h>

#include <string.h>
#include <ftw.h>

static int rmfile(const char *pathname, const struct stat *sbuf, int type, struct FTW *ftwb)
{
	if (remove(pathname) < 0)
		return -1;

	return 0;
}

short rmdirectory(char *dir)
{
	if (nftw(&dir[0], rmfile, 10, FTW_DEPTH|FTW_MOUNT|FTW_PHYS) < 0)
		return 1;

	return 0;
}

int main(int argc, char *argv[])
{
	short i;

	if (argc == 1) {
		fprintf(stderr, "Usage: rm [-r] file(s)\n");
		exit(-1);
	}

	else {
		if (strncmp(argv[1], "-r", 2) == 0)
			for (i = 1; i < argc; i++)
				rmdirectory(argv[i]);

		else
			for (i = 1; i < argc; i++)
				remove(argv[i]);
	}

	return 0;
}

#include <stdio.h>
#include <string.h>

#include <openssl/bio.h>
#include <openssl/err.h>

#include <openssl/ssl.h>
#include "config.h"

BIO *bio = NULL;
BIO *out = NULL;

void open_connection()
{
	char host[17] = {0};

	/* copy port to host */
	strcpy(host, HOST[0]);
	strcat(host, ":443");

	/* setup SSL */
	SSL_library_init();
	SSL_load_error_strings();

	/* create new SSL_CTX object to establish TLS/SSL connect */
	SSL_CTX *ctx = SSL_CTX_new(TLS_client_method());

	if (ctx == NULL)
		ERR_print_errors_fp(stderr);

	/* create a new SSL bio chain and new SSL structure */
	bio = BIO_new_ssl_connect(ctx);
	SSL *ssl;

	/* retrive SSL pointer of BIO; if connection is blocked don't retry  */
	BIO_get_ssl(bio, &ssl);
	SSL_set_mode(ssl, SSL_MODE_AUTO_RETRY);

	/* set the hostname */
	BIO_set_conn_hostname(bio, host);

	/* attempt to make the connection; if connection fails print to stderr */
	if (BIO_do_connect(bio) <= 0) {
		BIO_free_all(bio);
		ERR_print_errors_fp(stderr);
	}
}

void close_connection()
{
	/* free up entire BIO chain */
	BIO_free_all(bio);
}

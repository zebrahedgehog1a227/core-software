#include <stdio.h>
#include <string.h>

#include "get.h"
#include "parse.h"

void add_binary(char *package)
{
	char package_path[512] = {0};
	char *filename;

	filename = parse_name(package);
	parse_path(package, package_path);

	printf("\n%s\n", package_path);
	get_package(package_path, filename);
}

#include <stdio.h>
#include <stdlib.h>

#include <openssl/bio.h>
#include <openssl/err.h>

#include "config.h"

extern BIO *bio;
extern BIO *out;

void get_package(char *package, char *filename)
{
	char contents[1536] = {0};
	short i;

	/* create GET request */
	if (BIO_printf(bio, "GET %s HTTP/1.1\r\nHost: %s\r\n\r\n", package, HOST[0]) == -1)
		fprintf(stderr, "could not create get request\n");

	/* read in contents */
	BIO_read(bio, contents, sizeof(contents));

	/* create file for contents */
	out = BIO_new_file(filename, "w");

	if (!out) {
		ERR_print_errors_fp(stderr);
		exit(-1);
	}

	/* write contents to file */
	BIO_write(out, contents, sizeof(contents));
}

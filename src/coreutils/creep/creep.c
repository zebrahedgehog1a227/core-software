#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>
#include "add.h"

#include "connection.h"

int main(int argc, char *argv[])
{
	short opt, i;

	while ((opt = getopt(argc, argv, ":bsduUi")) != -1) {
		switch(opt) {
			case 'b':
				for (i = 2; i < argc; i++) {
					open_connection();
					add_binary(argv[i]);
					close_connection();
				}
			break;

			case 's':
				printf("source add\n");
			break;

			case 'd':
				printf("delete package\n");
			break;

			case 'u':
				printf("check for updates\n");
			break;

			case 'U':
				printf("upgrade packages\n");
			break;

			case 'i':
				printf("search for package\n");
			break;

			case '?':
				printf("Usage: creep [-bsduUi] package(s)\n");
				exit(-1);
			break;
		}
	}

	return 0;
}

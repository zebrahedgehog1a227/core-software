#include <stdio.h>
#include <string.h>

#include "config.h"

/* gets the word after the last "/" in path (i.e. the program name) */
char *parse_name(char *path)
{
	const char delminiter = '/';
	char *filename = path;

	while (*path != '\0') {
		if (*path == delminiter)
			filename = path + 1;

		path++;
	}

	return filename;
}

/* appends package to PAGE creating path */
void parse_path(char *package, char path[])
{
	strcpy(path, PAGE[0]);
	strcat(path, package);
}

#include <stdio.h>
#include <stdlib.h>

#include <string.h>
#include <sys/wait.h>

#include <unistd.h>
#include "builtins/cd.h"

#include "builtins/quit.h"

/* global variables for shell builtins */
char *builtins[] = {
	"cd",
	"exit"
};

short (*builtin_functions[]) (char **) = {
	cd,
	quit
};

short number_of_builtins() {
	return sizeof(builtins) / sizeof(char *);
}

/* launch a program and wait for it to terminate */
short program_launch(char **args)
{
	pid_t pid = fork();
	int status;

	if (pid == 0) {
		if (execvp(args[0], args) == -1) {
			fprintf(stderr, "command not found\n");
			exit(-1);
		}
	}

	else {
		do {
			waitpid(pid, &status, WUNTRACED);
		} while (!WIFEXITED(status) && !WIFSIGNALED(status));
	}

	return 1;
}

/* execute program entered by the user */
short program_execute(char **args)
{
	short i;

	/* nothing was given */
	if (args[0] == NULL)
		return 1;

	for (i = 0; i < number_of_builtins(); i++) {
		if (strcmp(args[0], builtins[i]) == 0) {
			return (*builtin_functions[i])(args);
		}
	}

	return program_launch(args);
}

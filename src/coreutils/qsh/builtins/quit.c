#include <stdio.h>
#include <stdlib.h>

short quit(char **args)
{
	int status;

	if (args[0] == NULL)
		exit(EXIT_SUCCESS);

	else
		status = (int)args[0];
		exit(status);

	return 0;
}

#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>

short cd(char **args)
{
	char *home = getenv("HOME");

	if (args[1] == NULL) {
		if (chdir(home) != 0)
			fprintf(stderr, "chdir(); failed\n");
	}

	else {
		if (chdir(args[1]) != 0)
			fprintf(stderr, "chdir(); failed\n");
	}

	return 1;
}

#ifndef PROGRAM_H
#define PROGRAM_H

short program_launch(char **args);
short program_execute(char **args);

#endif

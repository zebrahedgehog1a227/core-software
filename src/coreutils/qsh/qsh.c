#include <stdio.h>
#include <stdlib.h>

#include <string.h>
#include "program.h"

#include "config.h"

/* read input from the user */
char *read_line()
{
	short buffer_size = 512, position = 0, c;
	char *buffer = malloc(sizeof(char) * buffer_size);

	if (!buffer) {
		fprintf(stderr, "malloc(); failed, could not allocate buffer\n");
		exit(-1);
	}

	while (1) {
		c = getchar();

		if (c == EOF)
			exit(EXIT_SUCCESS);

		else if (c == '\n') {
			buffer[position] = '\0';
			return buffer;
		}

		else
			buffer[position] = c;

		position++;

		/* reallocate if buffer size is exceeded */
		if (position >= buffer_size) {
			buffer_size += buffer_size;
			buffer = realloc(buffer, buffer_size);
		}
	}
}

/* split the line entered by the user into tokens */
char **split_line(char *line)
{
	#define DELIMITERS " \t\r\n\a"
	#define ADDITIONAL_MEMORY 512

	short buffer_size = 64, position = 0;
	char **tokens = malloc(buffer_size * sizeof(char*));

	char *token, **tokens_backup;

	if (!tokens) {
		fprintf(stderr, "malloc(); failed, could not allocate tokens\n");
		free(tokens_backup);
		exit(-1);
	}

	token = strtok(line, DELIMITERS);

	while (token != NULL) {
		tokens[position] = token;
		position++;

		/* reallocate if buffer size is exceeded */
		if (position >= buffer_size) {
			buffer_size += ADDITIONAL_MEMORY;
			tokens_backup = tokens;

			tokens = realloc(tokens, buffer_size * sizeof(char*));
		}

		token = strtok(NULL, DELIMITERS);
	}

	tokens[position] = NULL;
	return tokens;
}

/* input loop for getting commands and executing them */
int main()
{
	char *input;
	char **args;

	short status;

	do {
		printf("%s", prompt);
		input = read_line();

		args = split_line(input);
		status = program_execute(args);

		free(input);
		free(args);
	} while (status);

	return 0;
}

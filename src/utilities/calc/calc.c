#include <stdio.h>
#include <stdlib.h>

void evaluate(double num1, char operator, double num2)
{
	double answer;

	switch (operator) {
		case '+':
			printf("%.1lf + %.1lf = %.1lf\n", num1, num2, num1 + num2);
			break;

		case '-':
			printf("%.1lf - %.1lf = %.1lf\n", num1, num2, num1 - num2);
			break;

		case '*':
			printf("%.1lf * %.1lf = %.1lf\n", num1, num2, num1 * num2);
			break;

		case '/':
			printf("%.1lf / %.1lf = %.1lf\n", num1, num2, num1 / num2);
			break;

		default:
			fprintf(stderr, "Error: invalid operator\n");
			exit(-1);
	}
}

int main(int argc, char *argv[])
{
	if (argc < 3) {
		fprintf(stderr, "Usage: calc numeral operator numeral\n");
		exit(-1);
	}

	double num1 = atof(argv[1]), num2 = atof(argv[3]);
	evaluate(num1, *argv[2], num2);
}

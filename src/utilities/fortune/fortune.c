#include <stdio.h>
#include <stdlib.h>

#include <string.h>

int main(int argc, char *argv[])
{
	if (argc < 2) {
		fprintf(stderr, "Usage: fortune [-d] filename\n");
		exit(-1);
	}

	char c, delimiter, fortune[256];
	short lines = 0, i = 0, count = 0;

	FILE *file;

	if (strncmp(argv[1], "-d", 2) == 0) {
		file = fopen(argv[3], "r");
		delimiter = *argv[2];
	}

	else {
		file = fopen(argv[1], "r");
		delimiter = '\n';
	}

	if (file == NULL) {
		fprintf(stderr, "file doesn't exist\n");
		exit(-1);
	}

	while ((c = getc(file)) != EOF) {
		if (strncmp(&c, &delimiter, 1) == 0)
			lines++;
	}

	rewind(file);
	i = rand() % lines;

	i = 14; /* DEBUG CODE FOR MULTILINE QUOTES (MY MULTILINE QUOTE IS ON LINE 14) */

	while (fgets(fortune, sizeof(fortune), file) != NULL) {
		if (count == i && fortune[strlen(fortune) - 2] == delimiter) {
			fortune[strlen(fortune) - 2] = '\0';
			printf("%s\n", fortune);

			break;
		}

		else
			count++;
	}

	fclose(file);
	return 0;
}
